FROM node:latest as builder
WORKDIR /var/www/api
COPY ./package*.json ./
RUN npm install
COPY . ./
RUN npm run build

FROM node:alpine
COPY ./package*.json ./
RUN npm install --production
COPY --from=builder /var/www/api/dist ./dist
CMD [ "npm", "start" ]