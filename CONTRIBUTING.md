# Contributing to Treviz

## Where to start

Thank you for your interest in Treviz ! There are many ways you can help us making this platform better.

If you want to contribute to the project, we can only encourage you to join us by registering on the
[Treviz](https://app.treviz.org) website (a public instance of Treviz), and candidate to Treviz.
This way, you will actually be rewarded for your work, and have access to all our Kanban boards, chatrooms, etc.
Just say you would love to contribute, specify your skills and preferences (frontend, backend, design...)
and we will gladly welcome you !

You can also have a look at the To Do list written in the readme file to find some general
guidelines regarding the work ahead. We would also love to receive any other contribution,
including:
* Translation of the administration module
* Documentation improvements
* Bug reports
* ...

## Bugs

If you have found a bug in Treviz, you can report it by opening an issue on GitHub. Specify exactly how
we can reproduce the issue, what is your setup (operating system, browser, php version, etc). The more information
we have about it, the faster we can track and resolve it.

## Contributing to the development

All the Treviz modules can be developed locally. For more instructions on how to install a Treviz instance on a
development machine or server, check out [our installation guide](doc.treviz.xyz). You can also run this service
independently with the following command line:

```
npm start
```

Create your own branch out of the dev branch of the repository, and prefix it with:
* 'feature/' if your work is related to a feature
* 'fix/' if your branch aims to fix an issue

During your development, try to separate the features of the application as much as you can. Controllers go into a specific
directory, so do all utilitarian functions, etc.
 
Once you are satisfied with the changes you made, detail thems in the [Changelog](./CHANGELOG.md). Once done, push your 
code, and feel free to open a Pull Request. In your pull request, you should:
* specify the changes you made
* justify those changes
* explain your design choices
* specify any potential drawback
* mention the issues that could be solved by your code

We'll normally let you know if your issue was accepted under a day.

In your code, please try to respect as possible the [Coding guidelines for TypeScript](https://github.com/Microsoft/TypeScript/wiki/Coding-guidelines).


## Licence

Any contribution must be licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0).
Read [our licence](./LICENCE.md), or ask us if you have any question.

## Code of conduct
    
As we develop and maintain this project, we aim at keeping its community as open and welcoming as possible. Stay
respectful of anyone who would like to contribute to it.

We do not tolerate:
* Harassment and discrimination of any kind
* Sexualized imagery and language
* Personal attacks
* Publishing other people's private information without explicit consent
* Spoilers for any show or movie

Any contribution, message, comment, code, issue or commit that would not respect this code will be removed, regardless
of its content.

If you have found any contribution to be offense, or that would break this code, please contact us so that we can remove
it.

## Contact
If you want to contribute, or have any question, you can also join us [by mail](mailto:bastien@treviz.xyz). We
usually reply in less than a day.
