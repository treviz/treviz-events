# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.3] - 2019-09-14
### Refactor
- Use a specific RabbitMQ exchange to retrieve notifications
- Use Gitlab CI to build and push images to registry

## [1.0.2] - 2018-11-04
### Added
- Docker-compose file with labels for easy deployment with [Traefik](http://traefik.io/)

## [1.0.1] - 2018-10-30
### Added
- Specify the port of the RabbitMQ instance in the configuration

### Fixed
- Stop listening to messages from a RabbitMQ queue when the user has disconnected 


## [1.0.0] - 2018-10-14
### Added
- Create microservice
- Listen to RabbitMQ for messages
- Send messages to client application using [EventSource](https://developer.mozilla.org/en-US/docs/Web/API/EventSource)
- Authenticate calls using JWT