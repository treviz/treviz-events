Treviz Events
========================

![Treviz Logo](https://treviz.xyz/build/images/treviz-logo-circle.png)

## Description

Treviz is an open-source web platform that helps you set up and join open, collaborative organizations, in which you 
can work on the projects you want and get rewarded for your work.

This repository contains the code of micro-service that handles server-sent events. It is developed using the
[Express microframework](http://expressjs.com), and uses [RabbitMQ](https://www.rabbitmq.com/) as a message broker (but
you can definitely replace it with any other amqp server). The language used is [Typescript][typescriptlang.org].

You may use this service independently from any Treviz instance, for instance if you want to add some real-time features
to your own application.

## Features
This service is used to perform real-time features:
* send events caught from RabbitMq to clients using [EventSource](https://developer.mozilla.org/en-US/docs/Web/API/EventSource)
* authenticate calls based on [JWT](http://jwt.io/)

We can separate this service in two elements:
* The controllers that are used to send messages to clients, located in the `src/controllers` folder
* Utilitarian functions, like JWT validation or RabbitMQ subscription, located in the `src/util` folder

Treviz is still in beta, so use it carefully.

## Installation

See our [documentation](https://doc.treviz.xyz) to learn more about how to install Treviz on your
own server.

## Setup the development environment

**Requirements**

* NodeJS >= 0.12

Simply clone this repository, and execute these commands: 

```
npm install
npm run serve
```

You can also use Docker to launch the microservice.

## Deploying the API

The API can easily be deployed on a server using [Traefik](http://traefik.io/). To do so, clone the project, and copy the `.env.dist` file
as `.env`. Then, fill it with the specifics attributes of your environment. The `PUBLIC_KEY_PATH` variable should match the path, on your
server, of the public key that will be used to validate JWT, and authenticate users. It should also be noted that we expect a rabbitmq
instance to be already running as a docker container in the network `web`.

You can now launch a docker instance with

```
$ docker-compose up -d
```

## Usage

You can start a connection by making an http request to `/:username`, where `username` is the name of a RabbitMQ Queue. The webservice
expects a Json Web Token to be sent as a `Bearer` token in the `Authorization` header. This token is then validated using the public key
specified when running the node server. The `username` contained in the JWT should match the name of the queue to listen to. 

Once the connection is established, it writes a dummy response to notify the client, then sends any message that is received on the RabbitMQ
queue.

## Contributing

Check out our [Contributing Guide](./CONTRIBUTING.md) if you are interested in joining the development team, if you want
to submit a bug, etc.

## Public instances

The list of public Treviz instances is available on [our website](https://treviz.xyz/instances). If you want to
list your, simply add it to the form, we'll check it out and display it!

## Privacy

Privacy is one of our main concerns when it comes to developing Treviz. We do not sell any information about our users, nor
do we try to analyze their behavior. If a security breach was to be found, we would hunt it down and fix it as soon as we could.

## Contact

You can learn more about Treviz on:
* our website: [treviz.xyz](https://treviz.xyz)
* our documentation [doc.treviz.xyz](https://doc.treviz.xyz)
* our blog [blog.treviz.xyz](https://blog.treviz.xyz)

Feel free to [contact us](https://treviz.xyz/contact) if you have any question.
