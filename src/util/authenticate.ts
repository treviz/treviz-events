import fs from 'fs'
import { verify } from 'jsonwebtoken'
import CONFIG from '../config'
import logger from '../util/logger'

const TAG = 'Authentication Service'

/**
 * Authenticate a user based on the JWT that was sent in the header.
 * @param username Username of the user to authenticate
 * @param token JWT given in the Authorization header to compare against
 */
export function authenticate(username: string, token: string): boolean {

    const cert = fs.readFileSync(process.env.PUBLIC_KEY_PATH || CONFIG.pubKeyPath)

    try {
        const decodedToken = verify(token, cert) as any
        logger.log(TAG, 'Valid Token')
        return decodedToken.username === username
    }
    catch (e) {
        logger.err(TAG, 'Invalid Token')
        return false
    }

}