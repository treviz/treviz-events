function log(tag: string, msg: string) {
    console.log(`${new Date().toISOString()} - ${tag}: ${msg}`)
}

function err(tag: string, msg: string) {
    console.error(`${new Date().toISOString()} - ${tag}: ${msg}`)
}

export default {
    log,
    err
}