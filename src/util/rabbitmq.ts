import amqplib, { Message, Options, Replies } from 'amqplib'
import { Connection, Channel } from 'amqplib'
import CONFIG from '../config'
import logger from './logger'

export class RabbitMQListener {

    private channel: Channel
    private consumerTag: string
    private alive = true

    private static connection: Connection;
    private static readonly EXCHANGE_NAME = 'treviz.notification'
    private static readonly TAG = 'RabbitMQListener'

    constructor() {
        if (RabbitMQListener.connection === null) {
            throw new Error('The connection to Rabbitmq is not established yet.')
        }
    }

    static connect() {
        const options: Options.Connect = {
            protocol: process.env.RABBITMQ_PROTOCOL         || CONFIG.amqpOptions.protocol,
            hostname: process.env.RABBITMQ_HOST             || CONFIG.amqpOptions.hostname,
            port:     Number(process.env.RABBITMQ_PORT)     || CONFIG.amqpOptions.port,
            username: process.env.RABBITMQ_USERNAME         || CONFIG.amqpOptions.username,
            password: process.env.RABBITMQ_PASSWORD         || CONFIG.amqpOptions.password,
        }

        amqplib
            .connect(options)
            .then((conn) => {
                logger.log(this.TAG, 'Connection to Rabbitmq established')
                RabbitMQListener.connection = conn
            })
            .catch((error) => {
                logger.err(this.TAG, `Connection to Rabbitmq failed: ${error}`)
            })
    }

    public listen(queue, handleMsg) {
        this.createChannel()
            .then(() => this.assertExchange())
            .then(() => this.assertQueue(queue))
            .then(() => this.consumeQueue(queue, handleMsg))
    }

    public close() {
        if (this.alive) {
            logger.log(RabbitMQListener.name, `Closing channel with tag: ${this.consumerTag}`)
            this.alive = false
            return this
                .channel
                .cancel(this.consumerTag)
                .then(() => logger.log(RabbitMQListener.TAG, `Channel successfully closed`))
                .catch((err) => logger.err(RabbitMQListener.TAG, `Closing channel failed: ${err}`))
        }
    }

    private createChannel() {
        return RabbitMQListener.connection
            .createChannel()
            .then((channel: Channel) => this.channel = channel)
    }

    private assertExchange() {
        return this
            .channel
            .assertExchange(RabbitMQListener.EXCHANGE_NAME, 'direct', {
                durable: true,
                autoDelete: false,
                internal: false
            });
    }

    private assertQueue(queue) {
        return this
            .channel
            .assertQueue(queue)
            .then(() => this.channel.bindQueue(queue, RabbitMQListener.EXCHANGE_NAME, queue))
    }

    private consumeQueue(queue, handleMsg) {
        return this
            .channel
            .consume(queue, (msg: Message) => {
                logger.log(RabbitMQListener.TAG, `Received message on channel with tag: ${this.consumerTag} | ${queue}`)
                try {
                    if (handleMsg(msg.content.toString())) {
                        this.channel.ack(msg)
                    }
                }
                catch (e) {
                    logger.err(RabbitMQListener.TAG, e)
                }
            })
            .then((consume: Replies.Consume) => {
                this.consumerTag = consume.consumerTag
                logger.log(RabbitMQListener.TAG, `Channel established for tag: ${this.consumerTag} | ${queue}`)
            })
    }
}