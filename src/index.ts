import express from 'express'
import CONFIG from './config'

import logger from './util/logger'
import { RabbitMQListener } from './util/rabbitmq'

import * as home from './controllers/home'
import * as events from './controllers/events'

const app = express()
const port = process.env.APP_PORT || CONFIG.port

const TAG = 'Treviz Events'

app.get('/', home.get)
app.get('/:username', events.get)

app.listen(port, () => {
    logger.log(TAG, `Service started and running on port ${port}`)
    RabbitMQListener.connect()
})