import { Request, Response } from 'express'
import logger from '../util/logger'

const TAG = 'Home Controller'

export function get(req: Request, res: Response) {
    logger.log(TAG, `GET /`)

    res.send({
        name: 'Treviz Events',
        version: '1.0'
    })
}