import { Request, Response } from 'express'
import { RabbitMQListener } from '../util/rabbitmq'
import { authenticate } from '../util/authenticate'
import logger from '../util/logger'

const TAG = 'Events Controller'

export function get(req: Request, res: Response) {

    const username = req.params.username
    const jwt = req.query.jwt

    logger.log(TAG, `GET /${username}`)

    if (authenticate(username, jwt)) {
        logger.log(TAG, 'Authentication successful, setting up event-stream')
        let alive = true

        res.writeHead(200, {
            'Connection': 'keep-alive',
            'Content-Type': 'text/event-stream',
            'Cache-Control': 'no-cache',
            'Access-Control-Allow-Origin': '*'
        })

        res.write(`data: ${JSON.stringify({type: 'dummy', content: 'connection established'})} \n\n`)

        const listener = new RabbitMQListener()

        const handleMsg = (msg: string) => {
            alive = res.write(`data: ${msg} \n\n`)
            return alive || handleConnectionLost()
        }

        const aliveHandler = setInterval(() => {
            alive = res.write(`data: ${JSON.stringify({type: 'dummy', content: 'keep alive'})} \n\n`)
            if (!alive) { handleConnectionLost() }
        }, 5000)

        const handleConnectionLost = () => {
            clearInterval(aliveHandler)
            listener.close()
            return false
        }

        listener.listen(`notification.${username}`, handleMsg)

    } else {
        logger.err(TAG, 'Authentication failed')
        res.sendStatus(403)
    }

}
