const CONFIG = {
    port: 3000,
    amqpOptions: {
        protocol: 'amqp',
        hostname: 'rabbitmq',
        port: 5672,
        username: 'guest',
        password: 'guest'
    },
    pubKeyPath: '/var/www/api/src/config/public.pem'
}

export default CONFIG